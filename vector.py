class Vector:

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = yield

    def __str__(self):
        return "x - {}, y = {}".format(self.x, self.y)

    def __add__(self, other):
        add = Vector(x=(self.x + other.x),
                     y=(self.y + other.y))
        return add

    def __sub__(self, other):
        sub = Vector(x=(self.x - other.x),
                     y=(self.y - other.y))
        return sub

    def __mul__(self, other):
        mul = Vector(x=(self.x * other.x),
                     y=(self.y * other.y))
        return mul

    def __ne__(self):
        return self.x != self.y
