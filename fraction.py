class Fraction:

    def __init__(self, numerator: int = 1, denominator: int = 1):
        self.numerator = numerator
        self.denominator = denominator

    def __sub__(self, other):
        sub_1 = Fraction(numenator=(self.numerator - other.numerator),
                       denominator=(self.denominator - other.denominator))
        return sub_1

    def __add__(self, other):
        add_1 = Fraction(numerator=(self.numerator + other.numerator),
                       denominator=(self.denominator + other.denominator))
        return add_1

    def __mul__(self, other):
        mul_1 = Fraction(numerator=(self.numerator * other.numerator),
                       denominator=(self.denominator * other.denominator))
        return mul_1

    def __div__(self, other):
        div = Fraction(numerator=(self.numerator * other.denominator),
                       denominator=(self.denominator * other.numerator))
        return div

    def reduce(self):
        limit = min(self.denominator, self.numerator)
        for i in  reversed(range(2, limit +1)):
            if (self.numerator % i == 0) and (self.denominator % i == 0):
               self.numerator = self.numerator / i
               self.denominator = self.denominator / i

    def __str__(self):
        return "{}/{}".format(self.numerator, self.denominator)